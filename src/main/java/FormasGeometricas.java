
public class FormasGeometricas {
	
	public double circulo() {
		double pi = 3.14;
		double raio = 20;
		double areaCirculo = pi * (raio * raio);
		return areaCirculo;
	}
	
		
	public double retangulo() {
		double ladoA = 10;
		double ladoB = 20;
		double areaRetangulo = ladoA * ladoB;
		return areaRetangulo;
	}
	
	
	public double triangulo() {
		double base = 10;
		double altura = 5;
		double areaTriangulo = (base * altura) / 2;
		return areaTriangulo;
	}
	
	
	public double trianguloEq() {
		double lado = 5;
		double areaTrianguloEq = (Math.sqrt(3) * (lado * lado)) / 4;
		return areaTrianguloEq;
	}
	
	
	
	
	// CORRE��O IVAN -----------------------
	
	/**
	 * C�lculo da �rea do c�rculo
	 * @param raio medida em cm
	 * @return �rea do c�rculo em cm2
	 */
	public float areaCirculo(float raio) {
		float area = 0;
		area = (float)Math.PI * (raio * raio);
		return area;
	}
	
	
	/**
	 * C�lculo da �rea do ret�ngulo
	 * @param ladoA medida em cm
	 * @param ladoB medida em cm
	 * @return �rea do ret�ngulo em cm�
	 */
	public float areaRetangulo(float ladoA, float ladoB) {
		float area = 0;
		area = ladoA * ladoB;
		return area; 
	}
	
	
	/**
	 * C�lculo da �rea do tri�ngulo
	 * @param ladoA medida da base em cm
	 * @param ladoB medida da altura em cm
	 * @return �rea do tri�ngulo em cm�
	 */
	public float areaTrianguloRetangulo(float ladoA, float ladoB) {
		float area = 0;
		area = (ladoA * ladoB)/2;
		return area;
	}
	
	
	/**
	 * C�lculo da �rea do tri�ngulo equil�tero 
	 * @param lado medida em cm
	 * @return �rea do tri�ngulo equil�tero em cm
	 */
	public float areaTrianguloEquilatero(float lado) {
		float area = 0;
		area = (float)(Math.sqrt(3) * (lado * lado)) / 4;
		return area;
	}
	
}
