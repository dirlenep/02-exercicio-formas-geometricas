import static org.junit.Assert.*;

import org.junit.Test;

public class FormasGeometricasTest {
	
	FormasGeometricas formasTeste = new FormasGeometricas();

	@Test
	public void testCirculo() {
		double resultCirculo = formasTeste.circulo();
		assertEquals(1256.0, 1256, resultCirculo);
	}
	
	@Test
	public void testRetangulo() {
		double resultRetangulo = formasTeste.retangulo();
		assertEquals(200.0, 200, resultRetangulo);
	}

	@Test
	public void testTriangulo() {
		double resultTriangulo = formasTeste.triangulo();
		assertEquals(25.0, 25, resultTriangulo);
	}
	
	@Test
	public void testTrianguloEq() {
		double resultTrianguloEq = formasTeste.trianguloEq();
		assertEquals(10.825317547305483, 10, resultTrianguloEq);
	}
	
	
	
	// CORRE��O IVAN ----------------------------
	@Test
	public void testAreaCirculo() {
		FormasGeometricas calc = new FormasGeometricas();
		float area = calc.areaCirculo(10);
		assertEquals(314.1592, area, 0.0001);
	}
	
	@Test
	public void testAreaRetangulo() {
		FormasGeometricas calc = new FormasGeometricas();
		float area = calc.areaRetangulo(5, 10);
		assertEquals(50, area, 0.0001);
	}

	@Test
	public void testAreaTrianguloRetangulo() {
		FormasGeometricas calc = new FormasGeometricas();
		float area = calc.areaTrianguloRetangulo(5, 10);
		assertEquals(25, area, 0.0001);
	}
	
	@Test
	public void testAreaTrianguloEquilatero() {
		FormasGeometricas calc = new FormasGeometricas();
		float area = calc.areaTrianguloEquilatero(10);
		assertEquals(43.301, area, 0.001);
	}

}
